<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\User;

class UserAuthController extends Controller
{
	protected $registrar;

	public function __construct(AuthController $registrar)
	{
		$this->registrar = $registrar;
	}

    public function getRegister()
    {
    	return view('auth.register');
    }

    public function postRegister(RegisterRequest $request)
    {
		User::create([
			'name'	=>	$request['name'],
			'email'	=>	$request['email'],
			'password'	=>	bcrypt($request['password'])
		]);

		return redirect('articles');
   	}
    public function getLogin()
    {
    	return view('auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
    	$credentials = $request->only('name', 'password');

    	dd($credentials);
    }
}
