@extends('layout.app')

@section('content')	
	<h1>Articles</h1>
	<hr>
	@foreach( $articles as $article )
		
		<a href="{{ action('ArticleController@show', [$article->id]) }}"><h1>{{ $article->title }}</h1></a> 
		<small>posted by: <a href="{{ url('/profile', [$article->user->name]) }}">{{ $article->user->name }}</a></small>

		<p>{{ $article->content }}</p>

		<hr>

	@endforeach
@stop