<h1>Posts made in {{ $country->country }}</h1>

@foreach( $country->articles as $article )
	
	<h3> {{ $article->title }} </h3> <small>by: {{ $article->user->name }}</small>
	<p> {{ $article->content }} </p>

@endforeach