<!DOCTYPE html>
<html>
<head>
	<title>Something Wrong!</title>
	{!! Html::style('/css/bootstrap.min.css') !!}

	@yield('styles');
</head>
<body>
	<div class="container">
		@yield('content')
	</div>

	@yield('scripts')
</body>
</html>