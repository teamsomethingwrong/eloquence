@extends('layout.app')

@section('content')
	<article>
		<h1>{{ $article->title }}</h1>
		<p><small>posted by: <a href="{{ url('/profile', [$article->user->name]) }}">{{ $article->user->name }}</a></small></p>
	
		<p>{{ $article->content }}</p>

		<h3>Comments on {{ $article->title }}</h3>	
	</article>

	@foreach( $article->comments as $comment )

		<p> {{ $comment->body }} <small><strong> commented by: <a href="{{ url('profile', [$comment->user->name]) }}">{{ $comment->user->name }}</a> </strong></small> </p>

	@endforeach	

@stop
