@extends('layout.app')

@section('content')
	
	<h1>Edit: {{ $article->title }} </h1>
	<hr>

	{!! Form::model($article, ['method' => 'PATCH', 'action' => ['ArticleController@update', $article->id]]) !!}
		
		@include('forms.articles_form', ['submitButtonText'	=>	'Update Article'])
		
	{!! Form::close() !!}

	@include('errors.form_errors')

@stop