{!! Form::hidden('user_id', 1, ['class' => 'form-control']) !!} <!-- temporary!! -->

<div class="form-group">
	{!! Form::label('title', 'Title:') !!}
	{!!  Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('content', 'Content:') !!}
	{!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::submit($submitButtonText, ['class' => 'form-control btn btn-primary']) !!}
</div>