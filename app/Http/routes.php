<?php

use App\Article;
use App\User;
use App\Country;
use App\Comment;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function(){
	return Auth::user();
});

Route::resource('articles', 'ArticleController');

Route::get('/profile/{username?}', 'ProfileController@show');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::controllers([
   'password' => 'Auth\PasswordController',
]);

/*Route::get('/posts/country/{countryId?}', function( $countryId ){
	$country = Country::findOrFail( $countryId );
	return view( 'country_posts' )->with( 'country', $country );
});

Route::get('/new-comment', function(){
	$commentor = User::find(3); //get the commentor

	$article = Article::find(1);

	$comment = new Comment();
	$comment->body = 'Great story!';
	$comment->user_id = $commentor->id;

	$article->comments()->save($comment);
});*/