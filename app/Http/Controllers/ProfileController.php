<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class ProfileController extends Controller
{
    public function show( $username )
    {
    	$user = User::where('name', $username)->firstOrFail();

		return view('profile', compact('user'));
    }
}
