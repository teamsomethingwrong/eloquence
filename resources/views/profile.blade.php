@extends('layout.app')

@section('content')
	<h1> {{ $user->name }}'s Profile </h1>
	<p>country: {{ $user->country ? $user->country->country : 'No country' }}</p>
	<p>roles: </p>
	<ul>
		@foreach($user->roles as $role)
			<li> {{ $role->name }} </li>
		@endforeach
	</ul>
	<hr>

	@foreach( $user->articles as $article )
		
		<article>
			<a href="{{ url('articles', [$article->id]) }}"><h2> {{ $article->title }} </h2></a>
			<p> {{ $article->content }} </p>
		</article>

	@endforeach
	
	<hr>

	<h3>Comments on {{ $user->name }}</h3>	

	@foreach( $user->comments as $comment )

		<p> {{ $comment->body }} <small><strong> commented by: <a href="{{ url('profile', [$comment->user->name]) }}">{{ $comment->user->name }}</a> </strong></small> </p>	

	@endforeach
@stop