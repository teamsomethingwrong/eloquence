@extends('layout.app')

@section('content')
	<h1>Write a new article</h1>
	<hr>

	{!! Form::open(['url' => '/articles']) !!}
		<div class="form-group">
			{!! Form::label('title', 'Title:') !!}
			{!!  Form::text('title', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('content', 'Content:') !!}
			{!! Form::textarea('content', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::hidden('user_id', 1, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Add Article', ['class' => 'form-control btn btn-primary']) !!}
		</div>
		
	{!! Form::close() !!}

	@if($errors->any())
		<ul class="alert alert-danger">
			@foreach($errors->all() as $error)
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	@endif

@stop